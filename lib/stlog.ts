import {Server as HAPIServer} from "hapi";
import * as stream from "stream";

import stringify = require("fast-safe-stringify");

export type STLevel = "critical" | "error" | "warn" | "notice" | "info" | "log"
export type STData = Error | Object | string;

export const STLevel = {
	"Critical": "critical" as STLevel,
	"Error": "error" as STLevel,
	"Info": "info" as STLevel,
	"Log": "log" as STLevel,
	"Notice": "notice" as STLevel,
	"Warn": "warn" as STLevel
};

export class STLog {
	public level: STLevel;
	public tags: string[];
	public data: STData;
	public timestamp: Date;

	constructor(level: STLevel = STLevel.Log, tags: string[] = [], data: STData = {}, timestamp: Date = new Date()) {
		this.level = level;
		this.tags = tags;
		this.data = data;
		this.timestamp = timestamp;
	}

	public toString(): string {
		const timestamp = this.formatTimestamp(this.timestamp);
		const tags = this.tags.join(", ");
		const data = stringify(this.data);

		return `[${timestamp}] ${this.level} [${tags}] data: ${data}`;
	}

	protected formatTimestamp(timestamp: Date): string {
		const year = timestamp.getFullYear().toString().slice(-2);
		const month = this.prependZero((timestamp.getMonth() + 1).toString(), 2);
		const day = this.prependZero(timestamp.getDate().toString(), 2);
		const hour = this.prependZero(timestamp.getHours().toString(), 2);
		const minute = this.prependZero(timestamp.getMinutes().toString(), 2);
		const second = this.prependZero(timestamp.getSeconds().toString(), 2);
		const milli = this.prependZero(timestamp.getMilliseconds().toString(), 3);

		return `${year}${month}${day}/${hour}:${minute}:${second}.${milli}`;
	}

	private prependZero(str: string, len: number): string {
		while (str.length < len) {
			str = "0" + str;
		}

		return str;
	}
}

export class STLogger extends stream.Readable {
	public level: STLevel;
	public tags: string[];

	constructor(level: STLevel = STLevel.Log, tags: string[] = ["*"], options: any = {}) {
		options.objectMode = true;
		super(options);

		this.level = level;
		this.tags = tags;
	}

	/*tslint:disable-next-line no-empty  */
	public _read(size: number): void {}

	public send(level: STLevel, tags: string[] = [], data: STData = {}, timestamp: Date = new Date()): void {
		if (limitLevel(this.level, level) && limitTags(this.tags, tags)) this.push(new STLog(level, tags, data, timestamp));
	}

	public critical(tags: string[] = [], data: STData = {}, timestamp: Date = new Date()): void {
		this.send(STLevel.Critical, tags, data, timestamp);
	}

	public error(tags: string[] = [], data: STData = {}, timestamp: Date = new Date()): void {
		this.send(STLevel.Error, tags, data, timestamp);
	}

	public warn(tags: string[] = [], data: STData = {}, timestamp: Date = new Date()): void {
		this.send(STLevel.Warn, tags, data, timestamp);
	}

	public notice(tags: string[] = [], data: STData = {}, timestamp: Date = new Date()): void {
		this.send(STLevel.Notice, tags, data, timestamp);
	}

	public info(tags: string[] = [], data: STData = {}, timestamp: Date = new Date()): void {
		this.send(STLevel.Info, tags, data, timestamp);
	}

	public log(tags: string[] = [], data: STData = {}, timestamp: Date = new Date()): void {
		this.send(STLevel.Log, tags, data, timestamp);
	}

}

export class STGoodTransform extends stream.Transform {

	constructor(options: any = {}) {
		options.objectMode = true;
		super(options);
	}

	public _transform(goodlog: any, encoding: string, callback: (err: Error|null, stlog: Object) => void): void {
		let tags: string[];
		let level: STLevel;
		let data: any;
		const timestamp = new Date(goodlog.timestamp);

		if (!goodlog.tags) {
			tags = [];
		} else if (Array.isArray(goodlog.tags)) {
			tags = goodlog.tags;
		} else {
			tags = [goodlog.tags];
		}

		if (goodlog.event === "error") {
			level = STLevel.Error;

			data = {
				"error": goodlog.error,
				"pid": goodlog.pid
			};

		} else if (goodlog.event === "ops") {
			level = STLevel.Info;
			tags = tags.concat("ops");

			data = {
				"host": goodlog.host,
				"pid": goodlog.pid,
				"os": goodlog.os,
				"proc": goodlog.proc,
				"load": goodlog.load
			};

		} else if (goodlog.event === "response") {
			level = STLevel.Log;
			tags = tags.concat("response");

			data = {
				"request_id": goodlog.id,
				"instance": goodlog.instance,
				"labels": goodlog.labels,
				"method": goodlog.method,
				"path": goodlog.path,
				"query": goodlog.query,
				"responseTime": goodlog.responseTime,
				"statusCode": goodlog.statusCode,
				"pid": goodlog.pid,
				"httpVersion": goodlog.httpVersion,
				"source": goodlog.source,
				"route": goodlog.route,
				"logs": goodlog.log,
				"config": goodlog.config
			};

		} else if (goodlog.event === "log") {
			level = STLevel.Log;
			tags = tags.concat("log");

			data = {
				"message": goodlog.data,
				"pid": goodlog.pid
			};

		} else {
			level = STLevel.Log;
			data = goodlog;
		}

		level = highestLevel(level, tags);

		callback(null, new STLog(level, tags, data, timestamp));
	}
}

export class STOutConsole extends stream.Writable {
	public level: STLevel;
	public tags: string[];

	constructor(level: STLevel = STLevel.Log, tags: string[] = ["*"], options: any = {}) {
		options.objectMode = true;
		super(options);

		this.level = level;
		this.tags = tags;
	}

	public _write(stlog: STLog): boolean {
		if (!limitLevel(this.level, stlog.level) || !limitTags(this.tags, stlog.tags)) return true;

		const out = stlog.toString();

		/* tslint:disable no-console*/
		switch (stlog.level) {
			case STLevel.Critical:
			case STLevel.Error:
				console.error(out);
				break;
			case STLevel.Warn:
				console.warn(out);
				break;
			case STLevel.Notice:
			case STLevel.Info:
				console.info(out);
				break;
			default:
				console.log(out);
		}
		/* tslint:enable no-console*/

		return true;
	}

}

export class STHapi extends stream.Writable {
	public level: STLevel;
	public tags: string[];

	private server: HAPIServer;

	constructor(server: HAPIServer, level: STLevel = STLevel.Log, tags: string[] = ["*"], options: any = {}) {
		options.objectMode = true;
		super(options);

		this.level = level;
		this.tags = tags;

		this.server = server;
	}

	public _write(stlog: STLog, encoding?: string, next?: Function): boolean {
		if (this.server.log) this.server.log([stlog.level, ...stlog.tags], stlog.data, stlog.timestamp.getTime());

		if (next) next();

		return true;
	}

}

function limitLevel(limit: STLevel, level: STLevel): boolean {
	if (!limit) return true;

	let validLevels = [STLevel.Critical];

	switch (limit) {
		case STLevel.Error:
			validLevels = [STLevel.Critical, STLevel.Error];
			break;
		case STLevel.Warn:
			validLevels = [STLevel.Critical, STLevel.Error, STLevel.Warn];
			break;
		case STLevel.Notice:
			validLevels = [
				STLevel.Critical, STLevel.Error,
				STLevel.Warn, STLevel.Notice
			];
			break;
		case STLevel.Info:
			validLevels = [
				STLevel.Critical, STLevel.Error,
				STLevel.Warn, STLevel.Notice,
				STLevel.Info
			];
			break;
		default:
			validLevels = [
				STLevel.Critical, STLevel.Error,
				STLevel.Warn, STLevel.Notice,
				STLevel.Info, STLevel.Log
			];
	}

	return (validLevels.indexOf(level) > -1);
}

function limitTags(valid: string[], tags: string[]): boolean {
	if (valid === null) return true;
	if (tags === null) return false;

	if (valid.indexOf("*") > -1) return true;
	if (valid.length === 0 || tags.length === 0) return false;

	return valid.some((value: string): boolean => (tags.indexOf(value) > -1));
}

function highestLevel(level: STLevel, tags: string[]): STLevel {
	if (level === STLevel.Critical || tags.indexOf(STLevel.Critical) > -1) {
		return STLevel.Critical;
	}

	if (level === STLevel.Error || tags.indexOf(STLevel.Error) > -1) {
		return STLevel.Error;
	}

	if (level === STLevel.Warn || tags.indexOf(STLevel.Warn) > -1) {
		return STLevel.Warn;
	}

	if (level === STLevel.Notice || tags.indexOf(STLevel.Notice) > -1) {
		return STLevel.Notice;
	}

	if (level === STLevel.Notice || tags.indexOf(STLevel.Notice) > -1) {
		return STLevel.Info;
	}

	return STLevel.Log;
}
