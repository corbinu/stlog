require("source-map-support").install();

const Lab = require("lab");
const expect = require("code").expect;

const moment = require("moment");
const Hapi = require("hapi");
const Boom = require("boom");

const STLog = require("../dist");
const Writer = require("./helpers/writer");

const lab = exports.lab = Lab.script();

lab.experiment("STLog", () => {
	const ts = new Date();

	lab.test("construct", (done) => {

		new STLog.STLog("log", ["tag2", "tag3"], {
			"test": 1
		}, new Date());

		done();
	});

	lab.test("setup no parameters", (done) => {

		const stlog = new STLog.STLog();

		expect(stlog.level).to.be.a.string().to.equal("log");
		expect(stlog.tags).to.be.an.array().to.have.length(0);
		expect(stlog.data).to.be.an.object().to.equal({});
		expect(stlog.timestamp).to.be.a.date();

		done();
	});

	lab.test("setup parameters", (done) => {

		const stlog = new STLog.STLog("error", ["tag2", "tag3"], {
			"test": 1
		}, ts);

		expect(stlog.level).to.be.a.string().to.equal("error");
		expect(stlog.tags).to.be.an.array().to.include(["tag2", "tag3"]);
		expect(stlog.data).to.be.an.object().to.equal({"test": 1});
		expect(stlog.timestamp).to.be.a.date().to.equal(ts);

		done();
	});

	lab.test("toString", (done) => {

		const stlog = new STLog.STLog("log", ["tag2", "tag3"], {
			"test": 1
		}, ts);

		setTimeout(() => {
			expect(stlog.toString()).to.be.a.string().to.equal(`[${moment(ts).format("YYMMDD/HH:mm:ss.SSS")}] log [tag2, tag3] data: {\"test\":1}`);

			done();
		}, 10);
	});

});

lab.experiment("STLogger", () => {

	lab.test("construct", (done) => {

		new STLog.STLogger();

		done();
	});

	lab.test("log", (done) => {

		const logger = new STLog.STLogger();

		const logs = [];

		const ts = new Date("2005-05-12");

		logger.on('data', (log) => {
			logs.push(log);
		});

		logger.on('end', () => {
			expect(logs).to.have.length(6);

			expect(logs[0].level).to.be.a.string().and.to.equal("error");
			expect(logs[0].tags).to.be.an.array().to.have.length(1).and.to.contain("send_test");
			expect(logs[0].data).to.be.an.error("test error");
			expect(logs[0].timestamp).to.be.a.date();

			expect(logs[1].level).to.be.a.string().and.to.equal("critical");
			expect(logs[1].tags).to.be.an.array().to.have.length(2).and.to.contains(["critical_test", "tag_test"]);
			expect(logs[1].data).to.be.a.string().and.to.equal("test2");
			expect(logs[1].timestamp).to.be.a.date();

			expect(logs[2].level).to.be.a.string().and.to.equal("error");
			expect(logs[2].tags).to.be.an.array().to.have.length(0);
			expect(logs[2].data).to.be.a.string().and.to.equal("test3");
			expect(logs[2].timestamp).to.be.a.date();

			expect(logs[3].level).to.be.a.string().and.to.equal("warn");
			expect(logs[3].tags).to.be.an.array().to.have.length(1).and.to.contain("warn_test");
			expect(logs[3].data).to.be.a.string().and.to.equal("test4");
			expect(logs[3].timestamp).to.be.a.date();

			expect(logs[4].level).to.be.a.string().and.to.equal("notice");
			expect(logs[4].tags).to.be.an.array().to.have.length(0);
			expect(logs[4].data).to.be.an.object().and.to.equal({});
			expect(logs[4].timestamp).to.be.a.date().and.to.equal(ts);

			expect(logs[5].level).to.be.a.string().and.to.equal("info");
			expect(logs[5].tags).to.be.an.array().to.have.length(0);
			expect(logs[5].data).to.be.an.object().and.to.equal({
				"test": {
					"type": "info"
				}
			});
			expect(logs[5].timestamp).to.be.a.date();

			done();
		});

		logger.send("error", ["send_test"], new Error("test error"));
		logger.critical(["critical_test", "tag_test"], "test2");
		logger.error([], "test3");
		logger.warn(["warn_test"], "test4");
		logger.notice([], {}, ts);
		logger.info([], {
			"test": {
				"type": "info"
			}
		});

		logger.read();
		logger.read();
		logger.read();
		logger.read();
		logger.read();
		logger.read();

		logger.emit('end');
	});

	lab.test("limit level", (done) => {

		const logger = new STLog.STLogger(STLog.STLevel.Warn);

		const logs = [];

		logger.on('data', (log) => {
			logs.push(log);
		});

		logger.on('end', () => {
			expect(logs).to.have.length(4);

			expect(logs[0].level).to.be.a.string().and.to.equal("error");
			expect(logs[0].tags).to.be.an.array().to.have.length(1).and.to.contain("send_test");
			expect(logs[0].data).to.be.an.error("test error");
			expect(logs[0].timestamp).to.be.a.date();

			expect(logs[1].level).to.be.a.string().and.to.equal("critical");
			expect(logs[1].tags).to.be.an.array().to.have.length(2).and.to.contains(["critical_test", "tag_test"]);
			expect(logs[1].data).to.be.a.string().and.to.equal("test2");
			expect(logs[1].timestamp).to.be.a.date();

			expect(logs[2].level).to.be.a.string().and.to.equal("error");
			expect(logs[2].tags).to.be.an.array().to.have.length(0);
			expect(logs[2].data).to.be.a.string().and.to.equal("test3");
			expect(logs[2].timestamp).to.be.a.date();

			expect(logs[3].level).to.be.a.string().and.to.equal("warn");
			expect(logs[3].tags).to.be.an.array().to.have.length(1).and.to.contain("warn_test");
			expect(logs[3].data).to.be.a.string().and.to.equal("test4");
			expect(logs[3].timestamp).to.be.a.date();

			done();
		});

		logger.send("error", ["send_test"], new Error("test error"));
		logger.critical(["critical_test", "tag_test"], "test2");
		logger.error([], "test3");
		logger.warn(["warn_test"], "test4");
		logger.notice();
		logger.info([], {
			"test": {
				"type": "info"
			}
		});

		logger.read();
		logger.read();
		logger.read();
		logger.read();
		logger.read();
		logger.read();

		logger.emit('end');
	});

	lab.test("limit tags", (done) => {

		const logger = new STLog.STLogger(null, ["critical_test", "warn_test"]);

		const logs = [];

		logger.on('data', (log) => {
			logs.push(log);
		});

		logger.on('end', () => {
			expect(logs).to.have.length(2);

			expect(logs[0].level).to.be.a.string().and.to.equal("critical");
			expect(logs[0].tags).to.be.an.array().to.have.length(2).and.to.contains(["critical_test", "tag_test"]);
			expect(logs[0].data).to.be.a.string().and.to.equal("test2");
			expect(logs[0].timestamp).to.be.a.date();

			expect(logs[1].level).to.be.a.string().and.to.equal("warn");
			expect(logs[1].tags).to.be.an.array().to.have.length(1).and.to.contain("warn_test");
			expect(logs[1].data).to.be.a.string().and.to.equal("test4");
			expect(logs[1].timestamp).to.be.a.date();

			done();
		});

		logger.send("error", ["send_test"], new Error("test error"));
		logger.critical(["critical_test", "tag_test"], "test2");
		logger.error([], "test3");
		logger.warn(["warn_test"], "test4");
		logger.notice();
		logger.info(null, {
			"test": {
				"type": "info"
			}
		});

		logger.read();
		logger.read();
		logger.read();
		logger.read();
		logger.read();
		logger.read();

		logger.emit('end');
	});

});

lab.experiment("STHapi", () => {

	const hapi = new Hapi.Server();

	lab.test("construct", (done) => {

		new STLog.STHapi(hapi);

		done();
	});

	lab.test("log", (done) => {

		const logger = new STLog.STLogger();
		const hapilog = new STLog.STHapi(hapi);

		logger.pipe(hapilog);

		const logs = [];

		const ts = new Date("2005-05-12");

		hapi.on('log', (event, tags) => {
			logs.push({
				event, 
				tags
			});

			if (logs.length > 5) logger.emit('end');
		});

		logger.on('end', () => {
			expect(logs).to.have.length(6);

			expect(logs[0].event.timestamp).to.be.a.number();
			expect(logs[0].event.tags).to.be.an.array().to.have.length(2).and.to.contain(["error", "send_test"]);
			expect(logs[0].event.data).to.be.an.error("test error");
			expect(logs[0].event.internal).to.be.a.boolean().and.to.equal(false);
			expect(logs[0].tags).to.equal({
				"error": true,
				"send_test": true
			});

			expect(logs[1].event.timestamp).to.be.a.number();
			expect(logs[1].event.tags).to.be.an.array().to.have.length(3).and.to.contain(["critical", "critical_test", "tag_test"]);
			expect(logs[1].event.data).to.be.a.string("test2");
			expect(logs[1].event.internal).to.be.a.boolean().and.to.equal(false);
			expect(logs[1].tags).to.equal({
				"critical": true,
				"critical_test": true,
				"tag_test": true
			});

			expect(logs[2].event.timestamp).to.be.a.number();
			expect(logs[2].event.tags).to.be.an.array().to.have.length(1).and.to.contain(["error"]);
			expect(logs[2].event.data).to.be.a.string("test3");
			expect(logs[2].event.internal).to.be.a.boolean().and.to.equal(false);
			expect(logs[2].tags).to.equal({
				"error": true
			});

			expect(logs[3].event.timestamp).to.be.a.number();
			expect(logs[3].event.tags).to.be.an.array().to.have.length(2).and.to.contain(["warn", "warn_test"]);
			expect(logs[3].event.data).to.be.a.string("test4");
			expect(logs[3].event.internal).to.be.a.boolean().and.to.equal(false);
			expect(logs[3].tags).to.equal({
				"warn": true,
				"warn_test": true
			});

			expect(logs[4].event.timestamp).to.be.a.number().and.to.equal(ts.getTime());
			expect(logs[4].event.tags).to.be.an.array().to.have.length(1).and.to.contain(["notice"]);
			expect(logs[4].event.data).to.equal({});
			expect(logs[4].event.internal).to.be.a.boolean().and.to.equal(false);
			expect(logs[4].tags).to.equal({
				"notice": true
			});

			expect(logs[5].event.timestamp).to.be.a.number();
			expect(logs[5].event.tags).to.be.an.array().to.have.length(1).and.to.contain(["info"]);
			expect(logs[5].event.data).to.equal({
				"test": {
					"type": "info"
				}
			});
			expect(logs[5].event.internal).to.be.a.boolean().and.to.equal(false);
			expect(logs[5].tags).to.equal({
				"info": true
			});

			done();
		});

		logger.send("error", ["send_test"], new Error("test error"));
		logger.critical(["critical_test", "tag_test"], "test2");
		logger.error([], "test3");
		logger.warn(["warn_test"], "test4");
		logger.notice([], {}, ts);
		logger.info([], {
			"test": {
				"type": "info"
			}
		});
	});

});

lab.experiment("STGoodTransform", () => {
	const hapi = new Hapi.Server();

	lab.test("construct", (done) => {

		new STLog.STGoodTransform();

		done();
	});

	lab.test("log", (done) => {
		const writer = new Writer(1);

		hapi.connection();

		hapi.route([
			{
				"method": 'GET',
				"path": '/success',
				"config": {
					"handler": (request, reply) => reply({"success": true})
				}
			},
			{
				"method": 'GET',
				"path": '/notfound',
				"config": {
					"handler": (request, reply) => reply(new Boom.notFound("not found", {"404": true}))
				}
			}
		]);

		hapi.register({
			"register": require("good"),
			"options": {
				"ops": false,
				"reporters": {
					"test": [
						new STLog.STGoodTransform(),
						writer
					]
				}
			}
		}, (err) => {
			if (err) return done(err);

			hapi.start();

			writer.on('end', () => {
				const logs = writer.data;

				expect(logs).to.have.length(4);

				expect(logs[0].level).to.be.a.string().and.to.equal("log");
				expect(logs[0].tags).to.be.an.array().to.have.length(2).and.to.contain(["info", "log"]);
				expect(logs[0].data.message).to.be.a.string().and.to.equal("test");
				expect(logs[0].data.pid).to.be.a.number();
				expect(logs[0].timestamp).to.be.a.date();

				expect(logs[1].level).to.be.a.string().and.to.equal("error");
				expect(logs[1].tags).to.be.an.array().to.have.length(2).and.to.contain(["error", "log"]);
				expect(logs[1].data.message).to.be.an.error();
				expect(logs[1].data.pid).to.be.a.number();
				expect(logs[1].timestamp).to.be.a.date();

				expect(logs[2].level).to.be.a.string().and.to.equal("log");
				expect(logs[2].tags).to.be.an.array().to.have.length(1).and.to.contain(["response"]);
				expect(logs[2].data.request_id).to.be.a.string();
				expect(logs[2].data.instance).to.be.a.string();
				expect(logs[2].data.labels).to.be.an.array().and.to.have.length(0);
				expect(logs[2].data.method).to.be.a.string().and.to.equal("get");
				expect(logs[2].data.path).to.be.a.string().and.to.equal("/success");
				expect(logs[2].data.query).to.equal({});
				expect(logs[2].data.responseTime).to.be.a.number();
				expect(logs[2].data.statusCode).to.be.a.number().and.to.equal(200);
				expect(logs[2].data.pid).to.be.a.number();
				expect(logs[2].data.httpVersion).to.be.a.string().and.to.equal("1.1");
				expect(logs[2].data.source).to.equal({
					"referer": undefined,
					"remoteAddress": "127.0.0.1",
					"userAgent": "shot"
				});
				expect(logs[2].data.route).to.be.a.string().and.to.equal("/success");
				expect(logs[2].data.logs).to.be.an.array().and.to.have.length(0);
				expect(logs[2].timestamp).to.be.a.date();

				expect(logs[3].level).to.be.a.string().and.to.equal("log");
				expect(logs[3].tags).to.be.an.array().to.have.length(1).and.to.contain(["response"]);
				expect(logs[3].data.request_id).to.be.a.string();
				expect(logs[3].data.instance).to.be.a.string();
				expect(logs[3].data.labels).to.be.an.array().and.to.have.length(0);
				expect(logs[3].data.method).to.be.a.string().and.to.equal("get");
				expect(logs[3].data.path).to.be.a.string().and.to.equal("/notfound");
				expect(logs[3].data.query).to.equal({});
				expect(logs[3].data.responseTime).to.be.a.number();
				expect(logs[3].data.statusCode).to.be.a.number().and.to.equal(404);
				expect(logs[3].data.pid).to.be.a.number();
				expect(logs[3].data.httpVersion).to.be.a.string().and.to.equal("1.1");
				expect(logs[3].data.source).to.equal({
					"referer": undefined,
					"remoteAddress": "127.0.0.1",
					"userAgent": "shot"
				});
				expect(logs[3].data.route).to.be.a.string().and.to.equal("/notfound");
				expect(logs[3].data.logs).to.be.an.array().and.to.have.length(0);
				expect(logs[3].timestamp).to.be.a.date();

				done();
			});

			hapi.log(["info"], "test");
			hapi.log(["error"], new Error("test error"));

			hapi.inject('/success', () => {

				hapi.inject('/notfound', () => {

					setTimeout(() => {
						writer.emit('end');
					}, 100);
				});
			});

		});
	});

});
