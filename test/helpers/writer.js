const Stream = require('stream');

class Writer extends Stream.Writable {
	constructor(expected) {
		super({ "objectMode": true });

		this.data = [];
		this.expected = expected;

		this.once('finish', () => {
			this._finalized = true;
		});
	}

	_write(chunk, end, callback) {
		this.data.push(chunk);

		if (this.data.length > this.expected-1) this.emit('finished');

		callback(null);
	}
}

module.exports = Writer;
